# Academic Writing

Repository for all my academic writing. Will not contain my thesis.

### __CV__

Curriculum Vitae. Last update January 9, 2019.

### __DOE_SCGSR_2017__

Application for DOE's SCGSR award. Applied for the Fall 2017 solicitation. Won - appointment from August 2018-2019.

### __NIU_dissertation_completion_2019__

Proposal for NIU's dissertation completion award. Deadline January 17, 2019.