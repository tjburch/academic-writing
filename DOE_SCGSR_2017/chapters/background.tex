	The proposed research concerns implementing Machine Learning for photon identification and isolation in the experiments at the Large Hadron Collider (LHC) at CERN. Photon identification within these experiments has been successful thus far, particularly at higher transverse energies. ATLAS, for example, has demonstrated a photon identification efficiency of over 97\% for photons with $E_{T} > 100$ GeV \cite{pileup}. This value falls off at lower $E_{T}$, though, and this regime is important for Higgs boson physics.

\begin{wrapfigure}{r}{0.45\textwidth}
   \includegraphics[width=0.45\textwidth]{../images/photon_pileup.pdf}
     \caption{Efficiency of unconverted photons candidates in the centermost region ($|\eta| < 0.6$) of ATLAS as a function of the number of reconstructed primary vertices, measured in 2012 ATLAS data from radiative Z boson decays \cite{pileup}. }
    \label{fig:pileup}
\end{wrapfigure}

 	The photon identification efficiency falls off as pile-up (the number of interactions per bunch crossing) increases, as shown in Figure \ref{fig:pileup}. ATLAS has already encountered runs with pile-up as high as 80. The pile-up will continue to increase in Run 3, and in the long term future with the HL-LHC, scheduled to be operational in 2025. The nominal luminosity for HL-LHC will correspond to a pile-up of $\langle\mu\rangle = 140$, but values as high as 200 are possible \cite{hl-lhc}. The photon identification efficiency in these conditions has been studied, and is projected to have a plateau efficiency of 76\%, shown in Figure \ref{fig:hl-lhc}. This is a rather low efficiency, and under 60 GeV, it falls off quite rapidly.

	In addition to identification, photon isolation also must be studied. Isolation cuts are set by reconstructing $E_{T}$ and $p_{T}$ in a cone around the photon candidate, defined by $R = \sqrt{\eta^{2}+\phi^{2}}$.  Smaller cones are more robust against energy depositions from pile-up, but larger cones will contain more energy, allowing for better vetoing of misidentified jets. Isolation variables have been studied and optimized, specifically with the trade-off between discrimination power and robustness against pile-up in mind \cite{ATL-PHYS-PUB-2011-007}.
	
	
	The rate at which photons are misidentified after applying identification and isolation requirements in an HL-LHC environment is shown in Figure \ref{fig:hl-lhc}. These misidentifications are called ``fakes'' and are caused by jets from quarks or gluons with a signature enough similar in the electromagnetic calorimeter to photons to pass the identification and isolation criteria. Additionally, real photons from the $\pi^0 \rightarrow \gamma \gamma$ decay are also classified as fakes. In the HL-LHC environment, the fake rate increases significantly - there is an order of magnitude more fakes than at low pileup. These new challenges in photon identification and isolation rising from future machine conditions motivate the investigation of alternative approaches. 

	
	There are many physics motivations for improving photon identification. Most notably is the diphoton Higgs decay channel. Additionally, Beyond Standard Model analyses using photons are ongoing. 	My recent work with the ATLAS collaboration has been on a di-Higgs analysis, focused on the $\gamma \gamma b \bar{b}$ final state, which is the topic of my doctoral thesis at Northern Illinois University (NIU). This involvement has motivated my interest in photon identification, since this channel is sensitive to the square of the photon identification efficiency.

\begin{figure}
%\centering
	\includegraphics[width=0.5\textwidth]{../images/fig_01.pdf} 
   \includegraphics[width=0.5\textwidth]{../images/fig_02a.pdf}
	  \caption{The efficiency of the photon identification and isolation requirements as a function of the true photon $p_{T}$ (left) and the fake rate after applying photon identification and isolation requirements as a function of true jet $p_{T}$ (right), both at $\langle \mu \rangle = 140$, extrapolated from $\langle \mu \rangle = 80$ \cite{hl-lhc}.}
	   \label{fig:hl-lhc}
\end{figure}

	The $\gamma\gamma b \bar{b}$ final state is interesting to study because we can search for resonances in a relatively clean environment. The large $h\rightarrow b\bar{b}$ branching ratio, clean diphoton trigger, diphoton invariant mass resolution, and low backgrounds make this channel appealing to search for pair production of Higgs bosons \cite{run1_bbyy}.  Observation, though, will require quite large integrated luminosities, which emphasizes the need for efficient photon identification in a high pile-up environment, such as at the HL-LHC.

	In the long term, results from this channel can lead to understanding electroweak symmetry breaking via studying the Higgs self-coupling. This uses the Higgs boson as a new tool for discovery in alignment with the goals laid out in the P5 report \cite{p5report}. While working toward that, we can search for new physics, looking for Higgs pair production from a heavy scalar or non-resonant, non-Standard Model like production, as well as gaining expertise in the $\gamma \gamma b \bar{b}$ final state.